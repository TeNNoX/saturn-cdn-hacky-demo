### Hacky demo I made while trying to figure out how filecoin saturn's browser-client works
Related issue:
https://github.com/filecoin-saturn/browser-client/issues/14

## TL;DR demo:
https://cool-colossal-square-goblin.fission.app/

### 1. load all scripts into the root folder for your domain
```
http -d https://saturn.tech/widget.js
http -d https://saturn.tech/saturn-sw.js
http -d https://saturn.tech/saturn-sw-core.js
```

### 2. loads the service worker and (in whatever way) load something from `/ipfs/` **after** the service worker is loaded
See [index.html](./index.html)

### 3. host your files on the root level of some domain that supports SSL (required for service worker)
For example, I used [my docker container of fission's cli](https://gitlab.com/txlab/docker/fission-cli/) to quickly upload to IPFS:
- `docker run -v $PWD:/app -w /app tennox/fission-cli`
- `fission setup`
- `fission app register` (dir: `.`, if you don't have your files in a subdirectory)

Visit the hosted page, e.g.:
https://cool-colossal-square-goblin.fission.app/
